package com.example.Assignment.config;


import com.example.Assignment.entity.StoreEntity;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;


@Configuration
@EnableBatchProcessing
@AllArgsConstructor
public class SpringBatchConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DataSource dataSource;

    //Batch -> Job Launcher->Job->Step->ItemReader->ItemProcessor->ItemWriter

    @Bean
    public Job job(JobBuilderFactory jobBuilderFactory,
                   StepBuilderFactory stepBuilderFactory,
                   ItemReader<StoreEntity> itemReader,
                   ItemProcessor<StoreEntity,StoreEntity> itemProcessor,
                   ItemWriter<StoreEntity> itemWriter){
        //Step

        Step step = stepBuilderFactory.get("Step1-Import Data").<StoreEntity,StoreEntity>chunk(100)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();

        return  jobBuilderFactory.get("Import Data From CSV")
                .incrementer(new RunIdIncrementer())
                .flow(step)
                .end()
                .build();
    }



    //ItemReader
    @Bean
    public ItemReader<StoreEntity> itemReader(){
        return  new FlatFileItemReaderBuilder<StoreEntity>()
                .resource(new ClassPathResource("store1.csv"))
                .name("file-reader")
                .targetType(StoreEntity.class)
                .delimited().delimiter(",")
                .names(new String[]{"storeid","storename","storetype","storeplace"})
                .build();
    }

    //ItemProcessor
    @Bean
    public ItemProcessor<StoreEntity, StoreEntity> itemProcessor(){
        return new ItemProcessor<StoreEntity, StoreEntity>() {
            @Override
            public StoreEntity process(StoreEntity store) throws Exception {
                StoreEntity updateStore = new StoreEntity();
                updateStore.setStoreid(store.getStoreid());
                updateStore.setStorename(store.getStorename());
                updateStore.setStoretype(store.getStoretype());
                updateStore.setStoreplace(store.getStoreplace());
                return updateStore;
            }

        };

    }

    //ItemWriter
    @Bean
    public JdbcBatchItemWriter<StoreEntity> writerIntoDB() {
        JdbcBatchItemWriter<StoreEntity> writer = new JdbcBatchItemWriter<StoreEntity>();
        writer.setDataSource(dataSource);
        writer.setSql("INSERT INTO store (storeid,storename,storetype,storeplace) VALUES (:storeid,:storename,:storetype,:storeplace)");
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<StoreEntity>());
        return writer;
    }





}


