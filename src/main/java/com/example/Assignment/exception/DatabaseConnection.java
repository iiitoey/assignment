package com.example.Assignment.exception;


import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class DatabaseConnection extends RuntimeException{
    private String message;
    public DatabaseConnection(String message) {
        super(message);
        this.message = message;
    }


}
