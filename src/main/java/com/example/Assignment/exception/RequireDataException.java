package com.example.Assignment.exception;

public class RequireDataException extends RuntimeException{
    private String message;

    public RequireDataException(String message){
        super(message);
        this.message = message;
    }
}
