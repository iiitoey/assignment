package com.example.Assignment.controller;

import com.example.Assignment.entity.StoreEntity;
import com.example.Assignment.exception.DatabaseConnection;
import com.example.Assignment.exception.RequireDataException;
import com.example.Assignment.service.StoreService;
import jdk.nashorn.internal.objects.NativeError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.BeanCreationException;

import java.util.List;
import java.util.Optional;
import java.lang.NullPointerException;


//GET POST PUT DELET
@RestController
@RequestMapping("/Store")
public class StoreController {

    @Autowired
    private final StoreService storeService;


    //
    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    //Get Find All Store
    @GetMapping(value = "/findAllStore")
    public List<StoreEntity> findAllStore(){
        return storeService.findAllStore();
    }


    //Get Store By Id
    @GetMapping(value = "/{storeId}")
    public Optional<StoreEntity> findById(@PathVariable("storeId") Integer storeId) throws DatabaseConnection {
        return storeService.findById(storeId);
    }

    //Createstore
    @PostMapping(value = "/createStore")
    public ResponseEntity<StoreEntity> saveStore(@RequestBody  StoreEntity storeEntity) throws RequireDataException {

        if (storeEntity.getStorename() == null || storeEntity.getStoretype() == null || storeEntity.getStoreplace() == null ){
            if (storeEntity.getStorename() == null){
                return new ResponseEntity("Api Require storeName",HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if (storeEntity.getStoretype() == null){
                return new ResponseEntity("Api Require storeType",HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if (storeEntity.getStoreplace() == null){

                return new ResponseEntity("Api Require storePlace",HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        storeService.saveStore(storeEntity);
        return new ResponseEntity<>(storeEntity,HttpStatus.CREATED);
    }


    //Update by Id
    @PutMapping(value="/{storeId}")
    public StoreEntity updateStore(@PathVariable("storeId") Integer storeId,@RequestBody StoreEntity storeEntity){
        storeEntity.setStoreid(storeId);
        return storeService.updateStore(storeEntity);
    }


    //Update Partiel
    @PatchMapping(value = "/updatePartial/{storeId}")
    public StoreEntity updatePartialById(@PathVariable("storeId") Integer storeId ,@RequestBody StoreEntity storeEntity){
        Optional<StoreEntity> storeDetail = storeService.findById(storeId);
        StoreEntity store =storeDetail.get();

        if (storeEntity.getStorename() != null){
            store.setStorename(storeEntity.getStorename());
        }
        if (storeEntity.getStoreplace() != null){
            store.setStoreplace(storeEntity.getStoreplace());
        }
        if (storeEntity.getStoretype() != null){
            store.setStoretype(storeEntity.getStoretype());
        }

        return storeService.updateStore(store) ;
    }

    //Delete
    @DeleteMapping(value = "/{storeId}")
    public void  deleteStore(@PathVariable("storeId") Integer storeId){
        storeService.deleteStore(storeId);
    }

}
