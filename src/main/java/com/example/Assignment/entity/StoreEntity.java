package com.example.Assignment.entity;


import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name="STORE")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class StoreEntity {

    @Id
    @GeneratedValue
    @Column(name="storeid")
    private int storeid;
    @Column(name="storename")
    private String storename;
    @Column(name="storetype")
    private String storetype;
    @Column(name="storeplace")
    private String storeplace;


    public int getStoreid() {
        return storeid;
    }

    public void setStoreid(int storeid) {
        this.storeid = storeid;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getStoretype() {
        return storetype;
    }

    public void setStoretype(String storetype) {
        this.storetype = storetype;
    }

    public String getStoreplace() {
        return storeplace;
    }

    public void setStoreplace(String storeplace) {
        this.storeplace = storeplace;
    }
}
