package com.example.Assignment.service;

import com.example.Assignment.entity.StoreEntity;

import java.util.List;
import java.util.Optional;

public interface StoreService {
    List<StoreEntity> findAllStore();
    Optional<StoreEntity> findById(Integer storeId);
    StoreEntity saveStore(StoreEntity storeEntity);
    StoreEntity updateStore(StoreEntity storeEntity);
    void deleteStore(Integer storeId);
}
