package com.example.Assignment.service.impl;

import com.example.Assignment.entity.StoreEntity;
import com.example.Assignment.exception.DatabaseConnection;
import com.example.Assignment.repository.StoreRepository;
import com.example.Assignment.service.StoreService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreServiceImpl implements StoreService {

    private final StoreRepository storeRepository;

    public StoreServiceImpl(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    @Override
    public List<StoreEntity> findAllStore() {
        return storeRepository.findAll();
    }

    @Override
    public Optional<StoreEntity> findById(Integer storeId) {
        try {
            return storeRepository.findById(storeId);
        }
        catch (Exception e){
            throw new DatabaseConnection("Cannot Connect Database");
        }
//        return storeRepository.findById(storeId);
    }


    @Override
    public StoreEntity saveStore(StoreEntity storeEntity) {
        return storeRepository.save(storeEntity);
    }

    @Override
    public StoreEntity updateStore(StoreEntity storeEntity) {
        return storeRepository.save(storeEntity);
    }

    @Override
    public void deleteStore(Integer storeId) {
        storeRepository.deleteById(storeId);
    }
}
