package com.example.Assignment.service.impl;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;

import java.io.IOException;

public class Consumer {

    private static final Logger logger = (Logger) LogManager.getFormatterLogger(Consumer.class);

    @KafkaListener(topics = "kafka", groupId = "myGroup")
    public void consume(String message) throws IOException {
        logger.info("Consume Message: %s", message);
    }
}
