package com.example.Assignment.repository;

import com.example.Assignment.entity.StoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface  StoreRepository extends JpaRepository<StoreEntity, Integer> {

    @Query(value = "SELECT * from STORE WHERE storeid = ?1",nativeQuery = true)
    Optional<StoreEntity> findById (Integer storeid);

}
