CREATE TABLE STORE (
   storeid serial NOT NULL  ,
   storename char(255) NOT NULL,
   storetype char(255) NOT NULL,
   storeplace char(255),
   PRIMARY KEY(storeid)
);