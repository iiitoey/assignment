package com.example.Assignment;

import com.example.Assignment.entity.StoreEntity;
import com.example.Assignment.repository.StoreRepository;
import com.example.Assignment.service.impl.StoreServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class StoreServiceTest {
    @Mock
    StoreRepository storeRepository;
    @InjectMocks
    StoreServiceImpl storeServiceImpl ;

    private StoreEntity store1,store2;
    @BeforeEach
    void setup()  throws  Exception{
        store1 = new StoreEntity() ;
        store1.setStoreid(200);
        store1.setStorename("Example1");
        store1.setStoretype("Example2");
        store1.setStoreplace("Example3");

        store2 = new StoreEntity() ;
        store2.setStoreid(201);
        store2.setStorename("Example4");
        store2.setStoretype("Example5");
        store2.setStoreplace("Example6");

    }

    @DisplayName("service_saveUser")
    @Test
    void service_saveUser() {
        given(storeRepository.save(store1)).willReturn(store1);

        System.out.println(storeRepository);
        System.out.println(storeServiceImpl);

        //when
        StoreEntity saveUser = storeServiceImpl.saveStore(store1);

        System.out.println(saveUser);

        //then
        assertThat(saveUser).isNotNull() ;
    }

    @DisplayName("Test_FindAllStore")
    @Test
    void TestFindAllStore() {
        StoreEntity store = new StoreEntity();
        store.setStoreid(202);
        store.setStorename("Example4");
        store.setStoretype("Example5");
        store.setStoreplace("Example6");
        List<StoreEntity> storeList =  new ArrayList<>();
        storeList.add(store);

        when(storeRepository.findAll()).thenReturn(storeList);

        List<StoreEntity> allStore = storeServiceImpl.findAllStore();
        assertThat(allStore.size()).isGreaterThan(0);

    }

    @DisplayName("TestUpdateStore")
    @Test
    void TestUpdateStore() {
        given(storeRepository.save(store1)).willReturn(store1) ;
        given(storeRepository.save(store2)).willReturn(store2) ;
        store1.setStorename("updateStoreName");
        store2.setStoretype("updateStoreType");


        StoreEntity updateStoretest = storeServiceImpl.updateStore(store1) ;
        StoreEntity updateStoretest2 = storeServiceImpl.updateStore(store2) ;

        System.out.println(updateStoretest);
        System.out.println(updateStoretest2);

        assertThat(updateStoretest).isNotNull();
        assertThat(updateStoretest2).isNotNull();

        assertThat(updateStoretest.getStorename()).isEqualTo("updateStoreName") ;
        assertThat(updateStoretest2.getStoretype()).isEqualTo("updateStoreType") ;

    }



    @DisplayName("TestFindById")
    @Test
    void TestFindById() {
        given(storeRepository.findById(200)).willReturn(Optional.of(store1)) ;
        //given(storeRepository.findById(300)).willReturn(Optional.of(store2)) ;

        StoreEntity findStoretest = storeServiceImpl.findById(store1.getStoreid()).get() ;
        //StoreEntity findStoretest2 = storeServiceImpl.findById(store2.getStoreid()).get() ;

        System.out.println(findStoretest);
        //System.out.println(findStoretest2);

        assertThat(findStoretest).isNotNull() ;
        //assertThat(findStoretest2).isNotNull() ;
    }


    @DisplayName("TestDeleteById")
    @Test
    void TestDeleteById() {
        int id = 200 ;
        willDoNothing().given(storeRepository).deleteById(id);

        // when -  action or the behaviour that we are going test
        storeServiceImpl.deleteStore(id);

        // then - verify the output
        verify(storeRepository,times(1)).deleteById(id);
    }

}
