package com.example.Assignment;


import com.example.Assignment.controller.StoreController;
import com.example.Assignment.entity.StoreEntity;
import com.example.Assignment.repository.StoreRepository;
import com.example.Assignment.service.StoreService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.ArrayList;
import java.util.List;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



@AutoConfigureMockMvc
@WebMvcTest(controllers = StoreController.class)
@ContextConfiguration(classes = {StoreController.class})
public class StoreControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    StoreService storeService;

    @Autowired
    private ObjectMapper objectMapper;

    @Mock
    private StoreRepository storeRepository;

    private StoreEntity store1,store2;

//    @BeforeEach
//    void setup() throws Exception{
//        store1 = new StoreEntity();
//        store1.setStoreid(101);
//        store1.setStorename("TestStoreName1");
//        store1.setStoretype("TestStoreType1");
//        store1.setStoreplace("TestStorePlace1");
//
//        store2 = new StoreEntity();
//        store2.setStoreid(102);
//        store2.setStorename("TestStoreName2");
//        store2.setStoretype("TestStoreType2");
//        store2.setStoreplace("TestStorePlace2");
//    }

    private List<StoreEntity> storeList;

    @BeforeEach
    void setup(){
        this.storeList = new ArrayList<>();
        this.storeList.add(new StoreEntity(101,"TestStoreName1","TestStoreType1","TestStorePlace1"));
        this.storeList.add(new StoreEntity(102,"TestStoreName2","TestStoreType2","TestStorePlace2"));
        this.storeList.add(new StoreEntity(103,"TestStoreName3","TestStoreType3","TestStorePlace3"));
    }

    @DisplayName("Test_Controller_FindAllStore")
    @Test
    void TestFindAllStore() throws Exception {

//        final Integer storeid = 104;
//        final StoreEntity store = new StoreEntity(104,"TestStoreName4","TestStoreType4"."TestStorePlace3");

        given(storeService.findAllStore()).willReturn(storeList);

        this.mockMvc.perform(get("/Store/findAllStore"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", CoreMatchers.is(storeList.size())));
    }

}
