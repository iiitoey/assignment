package com.example.Assignment;

import com.example.Assignment.entity.StoreEntity;
import com.example.Assignment.repository.StoreRepository;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StoreRepositoryTest {

    @Autowired
    private StoreRepository storeRepository;


    @Test
    @Rollback(value = false)
    public void saveStoreTest(){
        StoreEntity store = StoreEntity.builder()
                .storename("Test1")
                .storetype("Test2")
                .storeplace("Test3")
                .build();

        storeRepository.save(store);
        Assertions.assertThat(store.getStoreid()).isGreaterThan(0);
    }

    @Test
    public void findByIdTest(){
        StoreEntity store = storeRepository.findById(41).get();
        Assertions.assertThat(store.getStoreid()).isEqualTo(41);
    }

    @Test
    public void findAllTest(){
        List<StoreEntity> store = storeRepository.findAll();
        Assertions.assertThat(store.size()).isGreaterThan(0);
    }

    @Test
    public void updateTest(){
        StoreEntity store = storeRepository.findById(41).get();
        store.setStorename("Test1");
        store.setStoretype("Test2");
        store.setStoreplace("Test3");

        StoreEntity storeUpdate = storeRepository.save(store);
        Assertions.assertThat(storeUpdate.getStorename()).isEqualTo("Test1");
        Assertions.assertThat(storeUpdate.getStoretype()).isEqualTo("Test2");
        Assertions.assertThat(storeUpdate.getStoreplace()).isEqualTo("Test3");
    }

    @Test
    public void deleteTest(){
        StoreEntity store = storeRepository.findById(41).get();
        storeRepository.delete(store);

        StoreEntity store1 = null;
        Optional<StoreEntity> optinalStore = storeRepository.findById(1);

        if(optinalStore.isPresent()){
            store1 = optinalStore.get();
        }

        Assertions.assertThat(store1).isNull();
    }
}
