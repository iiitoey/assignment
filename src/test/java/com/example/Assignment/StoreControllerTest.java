package com.example.Assignment;

import com.example.Assignment.controller.StoreController;
import com.example.Assignment.entity.StoreEntity;
import com.example.Assignment.repository.StoreRepository;
import com.example.Assignment.service.StoreService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@WebMvcTest(controllers = StoreController.class)
@ContextConfiguration(classes={StoreController.class})
class StoreControllerTest {

    @Autowired
    private MockMvc mockMvctest ;

    @MockBean
    StoreService storeService ;
    @Autowired
    private ObjectMapper objmapper ;
    @Mock
    private StoreRepository storeRepository;

    private StoreEntity store1,storer2;
    @BeforeEach
    void setup() throws Exception {
        store1 = new StoreEntity() ;
        store1.setStoreid(200);
        store1.setStorename("Example1");
        store1.setStoretype("Example2");
        store1.setStoreplace("Example3");

        storer2 = new StoreEntity() ;
        storer2.setStoreid(201);
        storer2.setStorename("Example4");
        storer2.setStoretype("Example5");
        storer2.setStoreplace("Example6");

    }

    @DisplayName("controller_findAllUser")
    @Test
    void controller_findAllUser() throws Exception {

        //give
        List<StoreEntity> storeList = new ArrayList<>();

        given(storeService.findAllStore()).willReturn(storeList);

        this.mockMvctest.perform(get("/Store/findAllStore"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()",is(storeList.size())));
    }
    @DisplayName("controller_saveUser")
    @Test
    void controller_saveUser() throws Exception{

        given(storeService.saveStore(any(StoreEntity.class)))
                .willAnswer((invocation)-> invocation.getArgument(0));

        //when
        ResultActions response = mockMvctest.perform(post("/Store/createStore")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objmapper.writeValueAsString(store1)));

        //then
        response.andDo(print()).
                andExpect(status().isCreated()).andExpect(jsonPath("$.storename",
                        is(store1.getStorename())))
                .andExpect(jsonPath("$.storetype",
                        is(store1.getStoretype())))
                .andExpect(jsonPath("$.storeplace",
                        is(store1.getStoreplace())));;

    }
    @DisplayName("controller_findByID")
    @Test
    void controller_findUserById() throws Exception{

        //give
        int id = 200 ;
        given(storeService.findById(id)).willReturn(Optional.of(store1)) ;

        ResultActions response = mockMvctest.perform(get("/Store/{id}" , id));

        //then
        response.andDo(print()).
                andExpect(status().isOk()) .andExpect(jsonPath("$.storename",
                        is(store1.getStorename())))
                .andExpect(jsonPath("$.storetype",
                        is(store1.getStoretype())))
                .andExpect(jsonPath("$.storeplace",
                        is(store1.getStoreplace())));
    }
    @DisplayName("controller_updateUser")
    @Test
    void controller_updateUser() throws Exception{
        //give
        int id = 200 ;
        given(storeService.findById(id)).willReturn(Optional.of(store1)) ;

        given(storeService.updateStore(any(StoreEntity.class)))
                .willAnswer((invocation)-> invocation.getArgument(0));


        //when
        ResultActions response = mockMvctest.perform(put("/Store/{id}" , id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objmapper.writeValueAsString(store1)));


        //then
        response.andDo(print()).
                andExpect(status().isOk()) .andExpect(jsonPath("$.storename",
                        is(store1.getStorename())))
                .andExpect(jsonPath("$.storetype",
                        is(store1.getStoretype())))
                .andExpect(jsonPath("$.storeplace",
                        is(store1.getStoreplace())));


    }
    @DisplayName("controller_partialidUser")
    @Test
    void controller_partialidUser() throws Exception {
        int id = 200 ;
        given(storeService.findById(id)).willReturn(Optional.of(store1));

        store1.setStorename("Name_unittest_partialidUser");
        given(storeService.updateStore(any(StoreEntity.class)))
                .willAnswer((invocation)-> invocation.getArgument(0));

        //when
        ResultActions response = mockMvctest.perform(patch("/Store/updatePartial/{id}" , id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objmapper.writeValueAsString(store1)));
        //then
        response.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.storename", is(store1.getStorename())))
                .andExpect(jsonPath("$.storetype", is(store1.getStoretype())))
                .andExpect(jsonPath("$.storeplace", is(store1.getStoreplace())));
    }

    @DisplayName("controller_deleteUser")
    @Test
    void controller_deleteUser() throws Exception{
        int id = 300 ;
        willDoNothing().given(storeService).deleteStore(id);

        ResultActions response = mockMvctest.perform(delete("/Store/{id}" , id));

        //then
        response.andDo(print()).
                andExpect(status().isOk());
    }


}
